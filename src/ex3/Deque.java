package ex3;

public class Deque<E> implements DequeI<E> {

	public static void main(String[] args) {
		Deque<String> deque = new Deque<>();

		deque.addFirst("Hej1");
		deque.addFirst("Hej2");
		deque.addFirst("Hej3");
		deque.addLast("Hej4");
		deque.addLast("Hej5");
		deque.addLast("Hej6");
		deque.addFirst("Nyt indsæt");
		deque.removeFirst();
		deque.removeLast();
		System.out.println("Empty: " + deque.isEmpty());
		deque.toString();
		deque.clear();
		System.out.println("Empty: " + deque.isEmpty());
		System.out.println(deque.size());

	}

	// sentinels
	private Node header;
	private Node trailer;

	/**
	 * Creates an empty doubly-linked list.
	 */
	public Deque() {
		header = new Node(null);
		trailer = new Node(null);
		header.next = trailer;
		trailer.prev = header;
	}

	public void addFirst(E entry) {
		Node newNode = new Node(entry);
		//Hvis listen er tom
		if (header.next == trailer) {
			trailer.prev = newNode;
			newNode.next = trailer;
			header.next = newNode;
			newNode.prev = header;
		} else {
			newNode.next = header.next;
			header.next.prev = newNode;

			newNode.prev = header;
			header.next = newNode;

		}
	}

	public void addLast(E entry) {
		Node newNode = new Node(entry);
		//Hvis listen er tom
		if (header.next == trailer) {
			trailer.prev = newNode;
			newNode.next = trailer;
			header.next = newNode;
			newNode.prev = header;
		} else {
			trailer.prev.next = newNode;
			newNode.prev = trailer.prev;
			newNode.next = trailer;
			trailer.prev = newNode;
		}
	}

	public E removeFirst() {
		if (header.next == trailer) {
			System.out.println("Intet i deque");
		}
		E temp = header.next.data;
		header.next = header.next.next;
		header.next.prev = header;
		return temp;
	}

	public E removeLast() {
		if (header.next == trailer) {
			System.out.println("Intet i deque");
		}
		E temp = header.next.data;
		trailer.prev = trailer.prev.prev;
		trailer.prev.next = trailer;
		return temp;
	}

	public E getFirst() {
		if (header.next == trailer) {
			System.out.println("Intet i deque");
		}
		return header.next.data;
	}

	public E getLast() {
		if (header == trailer.prev) {
			System.out.println("Intet i deque");
		}
		return trailer.prev.data;
	}

	public boolean isEmpty() {
		if (header == trailer.prev) {
			return true;
		}
		return false;
	}

	public void clear() {
		while (header.next != trailer) {
			removeFirst();
		}
	}

	public int size() {
		int count = 0;
		Node node = header.next;
		while (node != trailer) {
			count++;
			node = node.next;
		}
		return count;
	}

	@Override
	public String toString() {
		Node node = header.next;
		while (node != trailer) {
			System.out.println(node.data);
			if (node.next != null) {
				node = node.next;
			} else {
				break;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------------

	private class Node {
		private E data;
		private Node next;
		private Node prev;

		public Node(E data) {
			this.data = data;
			next = null;
			prev = null;
		}
	}

}
