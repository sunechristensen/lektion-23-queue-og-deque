package ex3;

public class CircularDeque<E> implements DequeI<E> {
	public static void main(String[] args) {
		CircularDeque<String> cq = new CircularDeque<>();
		cq.addFirst("0");
		cq.addFirst("1");
		cq.addFirst("2");
		cq.addFirst("3");
		cq.addFirst("4");
		cq.addFirst("5");
		cq.addFirst("6");
		cq.addFirst("7");
		cq.addFirst("8");
		cq.addFirst("9");
		cq.addFirst("10");
		cq.addLast("sidst");
		cq.addLast("sidst2");
		System.out.println(cq.getLast());
		System.out.println(cq.getFirst());

		cq.toString();

	}

	// the index of the top entry in the stack,
	// top is 0 if the stack is empty
	private int head;
	private int trail;
	private int size;

	// contains the entries in the stack
	private E[] entries;

	// This method calls the constructor and set the size to 10
	public CircularDeque() {
		this(10);
	}

	public CircularDeque(int capacity) {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[capacity];
		entries = temp;
		head = 0;
		trail = 1;
	}

	public void extend() {
		@SuppressWarnings("unchecked")
		E[] extended = (E[]) new Object[entries.length * 2];
		int tempSize = size;
		for (int i = 0; !isEmpty(); i++) {
			extended[i] = removeFirst();
		}
		size = tempSize;
		entries = extended;
		trail = size;
		head = entries.length - 1;
	}

	public void addFirst(E entry) {

		if (head < 0) {
			head = entries.length - 1;
		}
		entries[head] = entry;
		head--;
		size++;
		if (size == entries.length) {
			extend();
		}
	}

	public void addLast(E entry) {
		if (isEmpty()) {
			trail--;
		}
		if (trail < entries.length) {
			entries[trail] = entry;
			trail++;
		} else if (trail == entries.length) {
			trail = 0;
			entries[trail] = entry;
			trail++;
		}
		size++;
		if (size > entries.length) {
			extend();
		}
	}

	public E removeFirst() {
		if (isEmpty()) {
			System.out.println("Deque er tomt");
		}
		if (head + 1 == entries.length) {
			head = -1;
		}
		E temp = entries[head + 1];
		entries[head + 1] = null;
		head++;
		size--;
		return temp;
	}

	public E removeLast() {

		if (isEmpty()) {
			System.out.println("Deque er tomt");
		}

		if (trail - 1 < 0) {
			trail = entries.length;
		}
		E temp = entries[trail - 1];
		entries[trail - 1] = null;
		trail--;
		size--;
		return temp;
	}

	public E getFirst() {
		return entries[head + 1];
	}

	public E getLast() {
		// TODO Auto-generated method stub
		return entries[trail - 1];
	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		}
		return false;
	}

	public void clear() {
		while (!isEmpty()) {
			removeFirst();
		}
	}

	public int size() {
		return size;
	}

	@Override
	public String toString() {
		for (E e : entries) {
			System.out.println(e);
		}
		return null;
	}

}
