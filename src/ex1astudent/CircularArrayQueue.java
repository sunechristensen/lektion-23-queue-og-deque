package ex1astudent;

import java.util.NoSuchElementException;

public class CircularArrayQueue<E> implements QueueI<E> {
	public static void main(String[] args) {
		CircularArrayQueue<String> circ = new CircularArrayQueue<>();

		circ.add("hej0");
		circ.add("hej1");
		circ.add("hej2");
		circ.add("hej3");
		circ.add("hej4");
		circ.add("hej5");
		circ.add("hej6");
		circ.add("hej7");
		circ.add("hej8");
		circ.clear();

		circ.add("hej9");

		circ.toString();
		System.out.println(circ.size);

	}

	// the index of the top entry in the stack,
	// top is 0 if the stack is empty
	private int head;
	private int trail;
	private int size;

	// contains the entries in the stack
	private E[] entries;

	// This method calls the constructor and set the size to 10
	public CircularArrayQueue() {
		this(10);
	}

	public CircularArrayQueue(int capacity) {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[capacity];
		entries = temp;
		head = 0;
		trail = 0;
	}

	public void add(E entry) {

		if (trail < entries.length) {
			entries[trail] = entry;
			trail++;

			/*
			 * denne else if sætning kan erstattes med
			 * tail = (tail+1) % entries.lenght;
			 * denne tæller op på tail indtil den når samme længde som
			 * entries.lenght, og bliver så sat til 0.
			 */
		} else if (trail == entries.length) {
			trail = 0;
			entries[trail] = entry;
			trail++;
		}
		size++;
		if (size == entries.length) {
			extend();
		}
	}

	public void extend() {
		@SuppressWarnings("unchecked")
		E[] extended = (E[]) new Object[entries.length * 2];
		int tracker = head;
		int i = 0;
		while (tracker != trail) {
			extended[i] = entries[tracker];
			i++;
			if (tracker == entries.length) {
				tracker = 0;
			} else {
				tracker++;
			}
		}
		entries = extended;
	}

	public E remove() {
		if (head >= trail) {
			throw new NoSuchElementException();
		}
		E temp = entries[head];
		if (head == entries.length) {
			head = 0;
			entries[head] = null;
		} else {
			entries[head] = null;
			head++;
		}
		size--;
		return temp;
	}

	public E element() {
		return entries[head];
	}

	public boolean isEmpty() {
		if (entries[head] == null) {
			return true;
		}
		return false;
	}

	public void clear() {
		while (!isEmpty()) {
			remove();
		}
		head = 0;
		trail = 0;
	}

	public int size() {
		return size;
	}

	@Override
	public String toString() {
		for (E e : entries) {
			System.out.println(e);
		}
		return null;
	}

}
