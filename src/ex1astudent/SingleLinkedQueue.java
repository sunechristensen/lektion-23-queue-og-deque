package ex1astudent;

import java.util.NoSuchElementException;

/*
 * Tidskompleksiteten  er O(1) hvis der ikke er loop
 * Tidskompleksiteten  er O(N) hvis der  er loop
 */

public class SingleLinkedQueue<E> implements QueueI<E> {

	public static void main(String[] args) {
		SingleLinkedQueue<String> queue = new SingleLinkedQueue<>();
		queue.add("hej1");
		queue.add("hej2");
		queue.add("hej3");
		queue.add("hej4");
		//		queue.toString();

		queue.remove();
		queue.remove();
		queue.remove();
		queue.remove();

		queue.toString();
		//		System.out.println(queue.size());
		//		System.out.println(queue.element());

		//Determine the time complexity of the queue's methods.
		//Er N^1 da der ikke er loops 
	}

	// first points to the node at the front of the queue
	// (= the first node in the singly-linked list)
	private Node first;

	// last points to the node at the back of the queue
	// (= the last node in the singly-linked list)
	private Node last;

	public SingleLinkedQueue() {
		first = null;
		last = null;
	}

	/**
	 * Adds a new entry to the back of the queue.
	 */
	@Override
	public void add(E entry) {
		Node newNode = new Node(entry);
		//Hvis listen er tom
		if (first == null) {
			first = newNode;
			newNode.next = last;
			last = newNode;
		}
		//Hvis kun 1 i liste
		else if (first.next == first) {
			first.next = newNode;
			last = newNode;
		} else {
			last.next = newNode;
			last = newNode;
		}
	}

	/**
	 * Removes and returns the entry at the front of the queue.
	 * Throws a NoSuchElementException(), if the queue is empty.
	 */
	@Override
	public E remove() {
		E removed = first.entry;
		if (first == null) {
			throw new NoSuchElementException();
		} else if (first == last) {
			first = null;
		} else {
			first = first.next;
		}
		return removed;
	}

	/**
	 * Returns the entry at the front of the queue.
	 * Throws a NoSuchElementException(), if the queue is empty.
	 */
	@Override
	public E element() {
		if (first == null) {
			throw new NoSuchElementException();
		} else {
			return first.entry;
		}
	}

	/**
	 * Returns true if the queue is empty.
	 */
	@Override
	public boolean isEmpty() {
		if (first != null) {
			return true;
		}
		return false;
	}

	/**
	 * Removes all entries from this queue.
	 */
	@Override
	public void clear() {
		while (first != null) {
			this.remove();
		}
	}

	/**
	 * Returns the number of elements in the queue.
	 */
	@Override
	public int size() {
		int count = 0;
		Node current = first;
		while (current != null) {
			count++;
			current = current.next;
		}
		return count;
	}

	@Override
	public String toString() {
		Node current = first;
		while (current != null) {
			System.out.println("\n" + current.entry);
			current = current.next;
		}
		return null;
	}
	//-------------------------------------------------------------------------

	private class Node {
		private E entry;
		private Node next;

		public Node(E entry) {
			this.entry = entry;
		}
	}

}
