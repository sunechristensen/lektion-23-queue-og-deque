package ex1astudent;

import java.util.ArrayDeque;

public class ArrayQueue<E> implements QueueI<E> {

	public static void main(String[] args) {
		ArrayDeque<String> deque = new ArrayDeque<>();
		deque.add("hej0");
		deque.add("hej1");
		deque.add("hej2");
		System.out.println(deque.size());
		deque.add("hej3");
		deque.add("hej4");
		deque.add("hej5");
		deque.add("hej6");
		deque.add("hej7");
		deque.add("hej8");
		deque.add("hej9");
		deque.add("hej9");
		deque.add("hej9");

		deque.remove();
		deque.remove();
		deque.remove();
		deque.remove();

		System.out.println(deque.toString());

		System.out.println(deque.isEmpty());
		deque.clear();
		System.out.println(deque.isEmpty());

	}

	private int top;

	// contains the entries in the stack
	private E[] entries;

	// This method calls the constructor and set the size to 10
	public ArrayQueue() {
		this(10);
	}

	public ArrayQueue(int capacity) {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[capacity];
		entries = temp;
		top = 0;
	}

	private void extendStackCapacity() {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[entries.length * 2];
		for (int i = 0; i < entries.length; i++) {
			temp[i] = entries[i];
		}
		entries = temp;
	}

	public void add(E entry) {
		entries[top] = entry;
		top++;
		if (top == entries.length) {
			extendStackCapacity();
		}
	}

	public E remove() {
		E temp = entries[0];
		for (int i = 0; i < top; i++) {
			entries[i] = entries[i + 1];
		}
		entries[top - 1] = null;
		top--;
		return temp;
	}

	public E element() {
		return entries[0];
	}

	public boolean isEmpty() {
		if (top == 0) {
			return true;
		}
		return false;
	}

	public void clear() {
		while (!isEmpty()) {
			remove();
		}
	}

	public int size() {
		return top - 1;
	}

	public String toString() {
		for (E e : entries) {
			System.out.println(e.toString());

		}
		return super.toString();
	}

}
