package ex1astudent;

public interface QueueI<E> {

    /**
     * Adds a new entry to the back of the queue.
     */
    public void add(E entry);

    /**
     * Removes and returns the entry at the front of the queue.
     * Throws a NoSuchElementException(), if the queue is empty.
     */
    public E remove();

    /**
     * Returns the entry at the front of the queue.
     * Throws a NoSuchElementException(), if the queue is empty.
     */
    public E element();

    /**
     * Returns true if the queue is empty.
     */
    public boolean isEmpty();

    /**
     * Removes all entries from this queue.
     */
    public void clear();

    /**
     * Returns the number of elements in the queue.
     */
    public int size();

}
