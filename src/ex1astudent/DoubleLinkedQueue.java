package ex1astudent;

import java.util.NoSuchElementException;

public class DoubleLinkedQueue<E> implements QueueI<E> {

	/*
	 * Tidskompleksiteten er O(1) hvis der ikke er loop
	 * Tidskompleksiteten er O(N) hvis der er loop
	 */

	public static void main(String[] args) {
		DoubleLinkedQueue<String> linkedqueue = new DoubleLinkedQueue<>();

		linkedqueue.add("hej1");
		linkedqueue.add("hej2");
		linkedqueue.add("hej3");
		linkedqueue.add("hej4");

		//		linkedqueue.remove();
		//		linkedqueue.remove();
		//		linkedqueue.remove();

		//		linkedqueue.clear();
		System.out.println("size: " + linkedqueue.size());

		linkedqueue.toString();
		System.out.println(linkedqueue.isEmpty());
		linkedqueue.clear();

	}

	// sentinels
	private Node header;
	private Node trailer;

	/**
	 * Creates an empty doubly-linked list.
	 */
	public DoubleLinkedQueue() {
		header = new Node(null);
		trailer = new Node(null);
		header.next = trailer;
		trailer.prev = header;
	}

	public void add(E entry) {
		Node newNode = new Node(entry);
		//Hvis listen er tom
		//		if (header.next == trailer) {
		//			trailer.prev = newNode;
		//			newNode.next = trailer;
		//			header.next = newNode;
		//			newNode.prev = header;
		//		} else {
		trailer.prev.next = newNode;
		newNode.prev = trailer.prev;
		newNode.next = trailer;
		trailer.prev = newNode;
		//		}
	}

	public E remove() {
		if (header.next == trailer) {
			System.out.println("Intet i deque");
			throw new NoSuchElementException();
		}

		Node remove = header.next;
		header.next = remove.next;
		header.next.prev = header;
		return remove.data;
	}

	public E element() {
		return header.next.data;
	}

	public boolean isEmpty() {
		if (header.next == trailer) {
			return true;
		} else {
			return false;
		}
	}

	public void clear() {
		Node current = header.next;
		while (current != trailer) {
			current = current.next;
			remove();
		}
	}

	/**
	 * Returns the number of elements in the list.
	 */
	public int size() {
		int count = 0;
		Node node = header.next;
		while (node != trailer) {
			count++;
			node = node.next;
		}
		return count;
	}

	@Override
	public String toString() {
		Node node = header.next;
		while (node != trailer) {
			System.out.println(node.data);
			if (node.next != null) {
				node = node.next;
			} else {
				break;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------------

	private class Node {
		private E data;
		private Node next;
		private Node prev;

		public Node(E data) {
			this.data = data;
			next = null;
			prev = null;
		}
	}
}
